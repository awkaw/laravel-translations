# Laravel Translations

## Installation

The library can be installed with Composer and is available on packagist under
[awkaw/laravel-translations](https://packagist.org/packages/awkaw/laravel-translations):

```bash
composer require awkaw/laravel-translations:^2.1
```

Run the migrations

```bash
php artisan migrate
```

## Install Google Chrome browser

```bash
docker run -p 9222:9222 --name your_name_for_image --restart always -d awkaw/chrome
```

## Usage

### Usage GoogleChromeBrowser


```php

use LaravelTranslations\Services\GoogleTranslateService;
use LaravelTranslations\Services\GoogleTranslate\Providers\GoogleChromeBrowser;

$sourceLocale = 'en';
$targetLocale = 'ru';

$googleChromeAddress = '127.0.0.1:9222'; // enter your address

$sourceText = 'Test translation';

$options = [
    "provider" => GoogleChromeBrowser::class,
    "googleChromeAddress" => $googleChromeAddress,
    "debug" => false,
];

$service = new GoogleTranslateService($sourceLocale, $targetLocale, $options);

$resultText = $service->translate($sourceText);
```