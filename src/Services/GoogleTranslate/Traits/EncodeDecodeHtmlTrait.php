<?php

namespace LaravelTranslations\Services\GoogleTranslate\Traits;

use Illuminate\Support\Str;

trait EncodeDecodeHtmlTrait
{
    protected $mapVars = [];

    protected function addToMapVars($key, $value){
        $this->mapVars[$key] = $value;
    }

    public function encode($text, $locale = null): string
    {

        $text = preg_replace('#&nbsp;#', ' ', $text);

        $this->mapVars = [];

        if(preg_match_all('#:[a-z]+#', $text, $matches)){

            if(isset($matches[0]) && !empty($matches[0])){

                foreach ($matches[0] as $match) {

                    $key = rand(10000, 40000);

                    $text = str_replace($match, $key, $text);

                    $this->mapVars[$key] = $match;
                }
            }
        }

        if(preg_match_all('#\{[a-z_]{3,40}\}#', $text, $matches)){

            if(isset($matches[0]) && !empty($matches[0])){

                foreach ($matches[0] as $match) {

                    $key = rand(40000, 90000);

                    $text = str_replace($match, $key, $text);

                    $this->mapVars[$key] = $match;
                }
            }
        }

        if(!empty($this->encoders)){

            $counter = 1000000;

            foreach ($this->encoders as $encoder) {

                if(isset($encoder['encoder']) && is_array($encoder['encoder']) && !empty($encoder['encoder'])){

                    foreach ($encoder['encoder'] as $from => $to) {

                        $text = preg_replace_callback('#'.$from.'#', function ($matches) use ($to, &$counter) {

                            if(isset($matches[1])) {

                                if(Str::contains($to, "key_value")){

                                    $counter++;

                                    $key = (string)$counter;

                                    $this->addToMapVars($key, $matches[1]);

                                    return str_replace("key_value", $key, $to);

                                }else if(Str::contains($to, "raw_value")){

                                    return str_replace("raw_value", $matches[1], $to);

                                }else{

                                    $charArray = [];

                                    $array = str_split($matches[1]);

                                    if(is_array($array) && !empty($array)){

                                        foreach ($array as $item) {
                                            $charArray[] = ord($item);
                                        }
                                    }

                                    return str_replace("value", implode("|", $charArray), $to);
                                }
                            }

                            return $matches[0];

                        }, $text);
                    }
                }
            }
        }

        return $text;
    }

    public function decode($text, $locale = null): string
    {
        $mapVarsUsed = [];

        if(!empty($this->encoders)){

            foreach ($this->encoders as $encoder) {

                if(isset($encoder['fix']) && is_array($encoder['fix']) && !empty($encoder['fix'])){

                    foreach ($encoder['fix'] as $from => $to) {
                        $text = preg_replace('#'.$from.'#', $to, $text);
                    }
                }

                if(isset($encoder['decoder']) && is_array($encoder['decoder']) && !empty($encoder['decoder'])){

                    $mapVars = $this->mapVars;

                    foreach ($encoder['decoder'] as $from => $to) {

                        $text = preg_replace_callback('#'.$from.'#', function ($matches) use ($from) {

                            $parts = explode("(.*?)", $from);

                            if(isset($matches[1]) && isset($parts[0]) && isset($parts[2])) {
                                return str_replace("\\", "", $parts[0]).str_replace(" ", "", $matches[1]).str_replace("\\", "", $parts[2]);
                            }

                            return $matches[0];

                        }, $text);

                        $text = preg_replace_callback('#'.$from.'#', function ($matches) use ($to, $mapVars, &$mapVarsUsed) {

                            if(isset($matches[1]) && Str::length($matches[1]) > 0) {

                                if(Str::contains($to, "key_value")){

                                    $result = "";

                                    $key = str_replace(" ", "", (string)$matches[1]);

                                    if(in_array($key, $mapVarsUsed)){
                                        return "";
                                    }

                                    if(isset($mapVars[$key])){

                                        $result = $mapVars[$key];

                                        $mapVarsUsed[] = $key;
                                    }

                                    return str_replace("key_value", $result, $to);

                                }else if(Str::contains($to, "raw_value")){

                                    return str_replace("raw_value", $matches[1], $to);

                                }else{

                                    $resultString = "";

                                    $charArray = explode("|", $matches[1]);

                                    if(is_array($charArray) && !empty($charArray)){

                                        foreach ($charArray as $item) {

                                            $item = (int)$item;

                                            if($item > 0){
                                                $resultString .= chr($item);
                                            }
                                        }
                                    }

                                    return str_replace("value", $resultString, $to);
                                }
                            }

                            return $matches[0];

                        }, $text);
                    }
                }
            }
        }

        if(!empty($this->mapVars)){

            foreach ($this->mapVars as $key => $value) {
                $text = str_replace($key, $value, $text);
            }
        }

        return $text;
    }
}
