<?php

namespace LaravelTranslations\Services\GoogleTranslate\Contracts;

interface ProviderContract
{
    public function __construct(string $source, string $target, array $options = null);
    public function translate(string $string): string;
}
