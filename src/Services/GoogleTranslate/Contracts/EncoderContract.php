<?php

namespace LaravelTranslations\Services\GoogleTranslate\Contracts;

interface EncoderContract
{
    public function encode($value, $locale = null):string;
    public function decode($value, $locale = null):string;
}
