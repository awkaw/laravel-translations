<?php

namespace LaravelTranslations\Services\GoogleTranslate\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use LaravelTranslations\Services\ContentFixedService\ContentFixedService;
use LaravelTranslations\Services\GoogleTranslate\Contracts\EncoderContract;
use LaravelTranslations\Services\GoogleTranslate\Contracts\ProviderContract;
use LaravelTranslations\Services\GoogleTranslate\Encoders\HtmlDog;
use LaravelTranslations\Services\GoogleTranslate\Encoders\HtmlExcMark;
use LaravelTranslations\Services\GoogleTranslate\Encoders\HtmlStars;
use LaravelTranslations\Services\GoogleTranslate\Encoders\HtmlStars2;
use LaravelTranslations\Services\GoogleTranslate\Encoders\Img;
use Stichoza\GoogleTranslate\GoogleTranslate;

class GoogleTranslateApi extends GoogleTranslate implements ProviderContract
{
    protected $options = [
        /*'timeout' => 10,
        'proxy' => [
            'http'  => 'tcp://localhost:8125',
            'https' => 'tcp://localhost:9124'
        ],*/
        'encoder' => "auto",
        'delay' => 2,
        'tries' => 3,
        'fixHtml' => true,
        'debug' => false,
        'headers' => [
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36'
        ]
    ];

    private $encoders = [
        "bg" => HtmlExcMark::class,
        "ru" => HtmlExcMark::class,
        "en" => HtmlExcMark::class,
        "nl" => Img::class,
        "ko" => Img::class,
        "cs" => Img::class,
        "es" => HtmlExcMark::class,
        "ja" => HtmlStars2::class,
        "pt" => HtmlExcMark::class,
        "sv" => HtmlExcMark::class,
        "hu" => HtmlExcMark::class,
        "de" => HtmlExcMark::class,
        "fr" => HtmlExcMark::class,
        "pl" => HtmlExcMark::class,
        "fi" => HtmlExcMark::class,
        "no" => HtmlExcMark::class,
        "da" => HtmlExcMark::class,
        "it" => HtmlStars2::class,
        "el" => HtmlExcMark::class,
        "th" => Img::class,
        "ro" => HtmlExcMark::class,
        "hr" => HtmlExcMark::class,
        "lt" => HtmlExcMark::class,
        "lv" => HtmlExcMark::class,
        "ar" => HtmlDog::class,
        "tr" => HtmlStars::class,
        "sl" => Img::class,
        "sk" => HtmlStars::class,
        "id" => HtmlExcMark::class,
        "zh-CN" => HtmlExcMark::class,
    ];

    protected array $replacesForLanguage = [];

    public function __construct(string $source, string $target, array $options = null)
    {
        if(is_string($target) && Str::length($target) > 0){
            $this->target = $target;
        }

        if(is_string($source) && Str::length($source) > 0){
            $this->source = $source;
        }

        if(is_array($options)){
            $this->options = array_merge_recursive_distinct($this->options, $options);
        }

        if(isset($this->options['delay']) && is_int($this->options['delay'])){
            $this->delay = $this->options['delay'];
        }

        if(isset($this->options['encoder']) && $this->options['encoder'] == "auto"){

            if(isset($this->encoders[$target])){
                $this->options['encoder'] = $this->encoders[$target];
            }else{
                $this->options['encoder'] = null;
            }
        }
    }

    public function registerEncoderForLanguage($encoder, $locale){
        $this->encoders[$locale] = $encoder;
    }

    public function translate(string $string): string{
        return $this->_translate($string, $this->options['delay'], 1);
    }

    private function _translate(string $string, $delay = null, $try = 1): string
    {
        $encoder = $this->options['encoder'];

        $encoderObj = (!is_null($encoder)) ? new $encoder : null;

        $result = "";

        $textForTranslate = $string;

        if(is_null($delay)){
            $delay = $this->options['delay'];
        }

        if($try == 1){

            if($this->options['fixHtml'] === true){
                $textForTranslate = ContentFixedService::fixHtml($textForTranslate);
            }

            if($encoderObj instanceof EncoderContract){
                $textForTranslate = $encoderObj->encode($textForTranslate, $this->target);
            }
        }

        if($this->options['debug']){
            Log::debug("Encoded source");
            Log::debug($textForTranslate);
        }

        try{

            if(is_int($delay) && $delay > 0){
                sleep($delay);
            }

            $result = (new GoogleTranslate($this->target, $this->source, [
                'headers' => $this->options['headers']
            ]))
                ->translate($textForTranslate);

            if($this->options['debug']){
                Log::debug("Encoded result");
                Log::debug($result);
            }

        }catch (\Exception $e){
            Log::debug($e->getMessage());
            Log::debug($e->getTraceAsString());
        }

        if($result === false || is_null($result) || Str::length($result) == 0){

            if($try == 1){

                // try 2

                $delay = $delay * 2;

                $result = $this->_translate($textForTranslate, $delay, 2);

                if($result === false || is_null($result)){

                    // try 3

                    $delay = $delay * 2;

                    $result = $this->_translate($textForTranslate, $delay, 3);
                }
            }

        }else{

            if($encoderObj instanceof EncoderContract){

                $result = $encoderObj->decode($result, $this->target);

                if($this->options['debug']){
                    Log::debug("Decode result");
                    Log::debug($result);
                }
            }
        }

        if($this->options['fixHtml'] === true){

            $result = ContentFixedService::fixHtml($result);

            if($this->options['debug']){
                Log::debug("Fixed result");
                Log::debug($result);
            }
        }

        /*if($try == 1){
            $result = $this->replaceHtmlTagsForLanguage($result, $this->target);
        }*/

        return $result;
    }

    public function setReplaceHtmlTagsForLanguage($target, $find, $replace){

        if(!is_array($this->replacesForLanguage)){
            $this->replacesForLanguage = [];
        }

        if(!isset($this->replacesForLanguage[$target])){
            $this->replacesForLanguage[$target] = [];
        }

        $this->replacesForLanguage[$target][$find] = $replace;
    }

    private function replaceHtmlTagsForLanguage($text, $target){

        $replaces = [];

        if(is_array($this->replacesForLanguage) && isset($this->replacesForLanguage[$target])){
            $replaces = $this->replacesForLanguage[$target];
        }

        if(is_array($replaces) && !empty($replaces)){

            foreach ($replaces as $find => $replace) {
                $text = str_replace($find, $replace, $text);
            }
        }

        return $text;
    }
}
