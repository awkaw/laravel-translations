<?php

namespace LaravelTranslations\Services\GoogleTranslate\Providers;

use GuzzleHttp\Psr7\Uri;
use HeadlessChromium\Browser;
use HeadlessChromium\Communication\Connection;
use HeadlessChromium\Communication\Message;
use HeadlessChromium\Page;
use http\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use LaravelTranslations\Services\GoogleTranslate\Contracts\EncoderContract;
use LaravelTranslations\Services\GoogleTranslate\Contracts\ProviderContract;
use LaravelTranslations\Services\GoogleTranslate\Encoders\GoogleChromeEncoder;
use LaravelTranslations\Services\GoogleTranslate\Encoders\GoogleChromeEncoderWithEncodeTags;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class GoogleChromeBrowser implements ProviderContract
{
    const MODE_TRANSLATE_AJAX = "translate_ajax";
    const MODE_TRANSLATE_IN_NEW_PAGE = "translate_new_page";
    const MAX_LENGTH_FOR_TRANSLATE = 5000;
    protected $source = "en";
    protected $target = "";
    protected $logger = null;
    protected $lastResult = "";
    protected $startPage = null;
    protected array $openPages = [];
    protected $encoder = null;

    protected array $options = [
        'googleChromeAddress' => '',
        'closeOtherPages' => true,
        'encoder' => 'auto',
        'encoderOptions' => [
            "encodeTagsForLanguages" => ["sr","da"] //,"ja","no"
        ],
        'fixHtml' => true,
        'debug' => false,
        'mode' => self::MODE_TRANSLATE_IN_NEW_PAGE,
        'delay' => 1,
        'tries' => 2,
        'headers' => [
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36'
        ]
    ];

    protected array $translateArray = [];

    public function __construct(string $source, string $target, array $options = null)
    {
        if(is_string($target) && Str::length($target) > 0){
            $this->target = $target;
        }

        if(is_string($source) && Str::length($source) > 0){
            $this->source = $source;
        }

        if(is_array($options)){
            $this->options = array_merge_recursive_distinct($this->options, $options);
        }

        if($this->options['encoder'] == "auto"){

            if(
                isset($this->options['encoderOptions']['encodeTagsForLanguages']) &&
                is_array($this->options['encoderOptions']['encodeTagsForLanguages']) &&
                in_array($target, $this->options['encoderOptions']['encodeTagsForLanguages'])
            ){
                $this->encoder = GoogleChromeEncoderWithEncodeTags::class;
            }else{
                $this->encoder = GoogleChromeEncoder::class;
            }
        }else{
            $this->encoder = $this->options['encoder'];
        }

        $this->logger = new \Monolog\Logger('debug');

        $this->logger->pushHandler(new StreamHandler(storage_path('logs/laravel.log')));
    }

    protected function closeOtherPages(Connection $connection){

        if(is_array($this->startPage)){

            $ids = [];

            foreach ($this->openPages as $openPage) {

                if($this->startPage['id'] != $openPage['id']){
                    $ids[] = $openPage['id'];
                }
            }

            if($this->options['debug']){
                Log::debug("closeOtherPages:");
                Log::debug(print_r($ids, true));
            }

            if(!empty($ids)){

                foreach ($ids as $id) {

                    try{

                        $connection->sendMessageSync(
                            new Message('Target.closeTarget', [
                                "targetId" => $id
                            ])
                        );

                    }catch (\Exception $e){

                    }
                }
            }
        }
    }

    protected function getOpenPages(){

        $chromeDevToolsUrl = new Uri(\sprintf('http://%s/json/list', $this->options['googleChromeAddress']));

        //$response = Http::get($chromeDevToolsUrl);
        //$parsedPages = \json_decode($response->body(), true);

        $response = (new \GuzzleHttp\Client())->get($chromeDevToolsUrl);
        $this->openPages = \json_decode($response->getBody(), true);

        return $this->openPages;
    }

    public function getStartPageUrl(){

        $this->startPage = null;

        $this->getOpenPages();

        if(is_array($this->openPages) && !empty($this->openPages)) {

            if (count($this->openPages) > 1 && $this->options['debug']) {
                print_r($this->openPages);
                print("\n");
            }

            $defaultPatternUrl = $patternUrl = '#\/welcome\/#';

            if($this->options['mode'] == self::MODE_TRANSLATE_AJAX){
                $patternUrl = '#translate\.google\.com#';
            }

            foreach ($this->openPages as $parsedPage) {

                if(isset($parsedPage['url']) && preg_match($patternUrl, $parsedPage['url'])){
                    $this->startPage = $parsedPage;
                }
            }

            if(is_null($this->startPage)){

                foreach ($this->openPages as $parsedPage) {

                    if(isset($parsedPage['url']) && preg_match($defaultPatternUrl, $parsedPage['url'])){
                        $this->startPage = $parsedPage;
                    }
                }
            }

            if(is_null($this->startPage)){
                $this->startPage = current($this->openPages);
            }
        }

        return $this->startPage;
    }

    public function translate(string $string): string{
        return $this->separate($string);
    }

    public function separate(string $string): string
    {
        if(
            Str::length($string) >= self::MAX_LENGTH_FOR_TRANSLATE &&
            preg_match("#<\/p>#is", $string)
        ){
            $separator = "</p>";

            $currentArray = explode($separator, $string);

            $allStr = "";

            $counter = 0;

            while (Str::length($string) > Str::length($allStr)){

                $currentStr = "";

                while (
                    $counter < count($currentArray) &&
                    isset($currentArray[$counter]) &&
                    (Str::length($currentStr.$currentArray[$counter].$separator)) <= self::MAX_LENGTH_FOR_TRANSLATE
                ){

                    $currentStr .= $currentArray[$counter].$separator;
                    $allStr .= $currentArray[$counter].$separator;
                    $counter++;
                }

                if(Str::length($currentStr) > 0){
                    $this->translateArray[] = $currentStr;
                }
            }

            if(!empty($this->translateArray)){
                $this->translateArray[count($this->translateArray)-1] = preg_replace("#".addslashes($separator)."$#is", "", $this->translateArray[count($this->translateArray)-1]);
            }

            if($this->options['debug']){
                Log::debug("=== Original texts for translate ===");
                Log::debug(print_r($this->translateArray, true));
            }

            $result = "";

            foreach ($this->translateArray as $key => $item) {

                $result .= $this->_translate($item);

                if($key > 0){
                    sleep(2);
                }
            }

        }else{

            $result = $this->_translate($string);
        }

        return $result;
    }

    public function _translate(string $string, $try = 1): string
    {
        $result = "";
        $html = "";

        if(Str::length($this->options['googleChromeAddress']) > 0){

            $page = null;
            $connection = null;
            $encoderObj = null;

            try{

                if(is_int($this->options['delay']) && $this->options['delay'] > 0){
                    sleep($this->options['delay'] * $try);
                }

                if(is_string($this->encoder) && Str::length($this->encoder) > 0){

                    $encoder = $this->encoder;

                    $encoderObj = new $encoder;
                }

                $textForTranslate = $string;

                if($this->options['debug']){
                    Log::debug("=== Original text for translate ===");
                    Log::debug($textForTranslate);
                    Log::debug("==========================");
                }

                if($this->options['fixHtml'] === true){

                    $textForTranslate = $this->fixHtml($textForTranslate);

                    if($this->options['debug']){
                        Log::debug("=== Original text for translate with fix Html ===");
                        Log::debug($textForTranslate);
                        Log::debug("==========================");
                    }
                }

                if($encoderObj instanceof EncoderContract){

                    if($this->options['encoder'] == "auto" && $encoderObj instanceof GoogleChromeEncoder){
                        $encoderObj->clearVars();
                    }

                    $textForTranslate = $encoderObj->encode($textForTranslate, $this->target);

                    if($this->options['debug']){
                        Log::debug("=== Encoded text for translate ===");
                        Log::debug($textForTranslate);
                        Log::debug("==========================");
                    }
                }

                if(Str::length($textForTranslate) > self::MAX_LENGTH_FOR_TRANSLATE){
                    throw new \Exception("Length more ".self::MAX_LENGTH_FOR_TRANSLATE." symbols");
                }

                $startPage = $this->getStartPageUrl();

                if(is_array($startPage)){

                    $connection = new Connection($startPage['webSocketDebuggerUrl'], null, 60 * 1000);
                    $connection->connect();

                    if(!$connection->isConnected()){
                        throw new \Exception("Error connect for {$startPage['webSocketDebuggerUrl']}");
                    }

                    if($this->options['closeOtherPages']){
                        $this->closeOtherPages($connection);
                    }

                    $browser = new Browser($connection);

                    $page = null;

                    if($this->options['mode'] == self::MODE_TRANSLATE_AJAX){

                        $target = $browser->getTarget($startPage['id']);

                        $frameTreeResponse = $target->getSession()->sendMessageSync(new Message('Page.getFrameTree'));

                        if(isset($frameTreeResponse['result']) && isset($frameTreeResponse['result']['frameTree'])){

                            $page = new Page($target, $frameTreeResponse['result']['frameTree']);
                        }
                    }

                    if($this->options['mode'] == self::MODE_TRANSLATE_IN_NEW_PAGE || !$page instanceof Page){

                        $page = $browser->createPage();

                        $page->setUserAgent($this->options['headers']['User-Agent']);
                    }

                    $url = "https://translate.google.com/?".http_build_query([
                            "sl" => $this->source,
                            "tl" => $this->target,
                            "text" => $textForTranslate,
                            "op" => "translate",
                        ]);

                    $page
                        ->navigate($url)
                        ->waitForNavigation()
                    ;

                    $wait = 0;
                    $maxWait = 10;
                    $step = 0.5;

                    while ($wait < $maxWait){

                        $html = $page->getHtml();

                        if(preg_match('#Agree to the use#is', $html)){
                            $page->evaluate("document.querySelector(\"button[aria-label^='Agree to the use']\").click()");
                        }

                        if(preg_match('#Accept all#is', $html)){
                            $page->evaluate("document.querySelector(\"button[aria-label^='Accept all']\").click()");
                        }

                        if(preg_match('#data-initial-value="(.*?)"#is', $html, $matches) && isset($matches[1])){

                            if($this->lastResult != $matches[1]){

                                $result = $matches[1];

                                break;
                            }
                        }

                        if(
                            preg_match_all('#data-text="(.*?)"#is', $html, $matches)
                            && isset($matches[1][1])
                            && !empty($matches[1][1])
                        ){

                            if(is_string($matches[1][1]) && Str::length($matches[1][1]) > 0){

                                if($this->lastResult != $matches[1][1]){

                                    $result = $matches[1][1];

                                    break;
                                }
                            }
                        }

                        $wait++;
                        usleep($step * 1000000);
                    }
                }

                if(is_string($result) && Str::length($result) > 0){
                    $this->lastResult = $result;
                }

                if($this->options['debug']){
                    Log::debug("=== Original Result translate ===");
                    Log::debug($result);
                    Log::debug("==========================");
                }

                if(is_string($result) && Str::length($result) > 0){

                    if($this->options['fixHtml'] === true){

                        $result = $this->fixHtml($result);

                        if($this->options['debug']){
                            Log::debug("=== Original Result translate with fix Html ===");
                            Log::debug($result);
                            Log::debug("==========================");
                        }
                    }
                }

                if($encoderObj instanceof EncoderContract){
                    $result = $encoderObj->decode($result, $this->target);
                }

                if($this->options['debug']){
                    Log::debug("=== Final Result translate ===");
                    Log::debug($result);
                    Log::debug("==========================");
                }

            } catch (\Exception $e){

                Log::debug($e->getMessage());
                Log::debug($e->getTraceAsString());

            } finally {

                try {

                    if($page instanceof Page && $this->options['mode'] == self::MODE_TRANSLATE_IN_NEW_PAGE) {
                        $page->close();
                    }

                    if($connection instanceof Connection){
                        $connection->disconnect();
                    }

                }catch (\Exception $e){

                    Log::debug($e->getMessage());
                    Log::debug($e->getTraceAsString());
                }
            }
        }

        if(!is_string($result)){
            $result = "";
        }

        return $result;
    }

    protected function fixHtml(string $html): string {

        $html = html_entity_decode($html);
        $html = htmlspecialchars_decode($html);

        $html = preg_replace('#\. jpg#is', '.jpg', $html);
        $html = preg_replace('#\. jpeg#is', '.jpeg', $html);
        $html = preg_replace('#\. png#is', '.png', $html);

        $html = preg_replace('#&amp;#is', '&', $html);
        $html = preg_replace('#&nbsp;#is', ' ', $html);
        $html = preg_replace('#&quot;#is', '"', $html);
        $html = preg_replace('#&\#039;#is', '\'', $html);
        $html = preg_replace('#&lt;#is', '<', $html);
        $html = preg_replace('#&gt;#is', '>', $html);

        $html = preg_replace('#< p#is', '<p', $html);
        $html = preg_replace('#< \\[p]#is', '<\p', $html);
        $html = preg_replace('#><#is', '> <', $html);
        $html = preg_replace('#< \/#is', '</', $html);
        $html = preg_replace('#<\/ #is', '</', $html);
        $html = preg_replace('#<\/p><#is', "</p>\n<", $html);
        $html = preg_replace('#<br><#is', "<br> <", $html);

        $html = preg_replace('#<[\s]{1,2}p>#is', '<p>', $html);
        $html = preg_replace('#<ahref#is', '<a href', $html);

        $html = preg_replace('#([\S+])[\s]{1,3}=[\s]{1,3}"#', "$1=\"", $html);
        $html = preg_replace('#([\S+])[\s]{1,3}="#', "$1=\"", $html);
        $html = preg_replace('#([\S+])=[\s]{1,3}"#', "$1=\"", $html);

        $html = preg_replace('#="_ ([\w]{1,20})"#is', '="_$1"', $html);

        foreach (["href","src"] as $urlAttribute) {

            $html = preg_replace_callback('#'.$urlAttribute.'="(.*?)"#is', function ($matches) use ($urlAttribute) {

                $result = $matches[0];

                if(isset($matches[1])){

                    $matches[1] = str_replace(" ", "", $matches[1]);

                    $result = "{$urlAttribute}=\"{$matches[1]}\"";
                }

                return $result;

            }, $html);
        }

        return $html;
    }
}
