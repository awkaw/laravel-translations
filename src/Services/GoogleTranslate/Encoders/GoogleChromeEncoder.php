<?php

namespace LaravelTranslations\Services\GoogleTranslate\Encoders;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use LaravelTranslations\Services\GoogleTranslate\Contracts\EncoderContract;

class GoogleChromeEncoder implements EncoderContract
{
    protected $vars = [];
    protected $urlAttributes = ["href","src","data-tag","data-ctag","style"];
    protected $encodeTags = ["figure"];

    public function clearVars(){
        $this->vars = [];
    }

    public function encode($value, $locale = null): string
    {
        // Fix html before encode
        $value = html_entity_decode($value);
        $value = htmlspecialchars_decode($value);

        $counter = 0;

        $value = preg_replace('#:([A-z][A-z_-]{1,50})#is', '{colon_$1_colon}', $value);
        $value = preg_replace('#\[([\w_-]{1,50})\]#is', '{quad_$1_quad}', $value);

        $value = preg_replace_callback('#\{([\w_-]{1,50})\}#is', function ($matches){

            $result = $matches[0];

            if(
                isset($matches[1]) &&
                !preg_match('#\{colon_#', $matches[0]) &&
                !preg_match('#\{quad_#', $matches[0])
            ){
                $result = "{encoded_".$matches[1]."_encoded}";
            }

            return $result;

        }, $value);

        while (preg_match('#\{([A-z]{1}[A-z0-9_-]{1,70})\}#is', $value, $matches)){

            $counter++;

            $this->vars["var{$counter}"] = $matches[0];

            //$value = Str::replaceFirst($matches[0], "{~{$counter}~}", $value);
            $value = Str::replaceFirst($matches[0], "{{$counter}}", $value);

            if($counter > 999){
                break;
            }
        }

        while (preg_match('#\{([A-z]{1}[A-Za-z0-9\._]{1,20}):#is', $value, $matches) && isset($matches[1])){

            $counter++;

            $this->vars["var{$counter}"] = $matches[1];

            //$value = Str::replaceFirst($matches[1], "{~{$counter}~}", $value);
            $value = Str::replaceFirst($matches[1], "{{$counter}}", $value);

            if($counter > 999){
                break;
            }
        }

        foreach ($this->encodeTags as $encodeTag) {

            $value = preg_replace('#<\/('.$encodeTag.')>#is', "<p data-ctag=\"{$encodeTag}\">", $value);
            $value = preg_replace('#<('.$encodeTag.')>#is', "<p data-tag=\"{$encodeTag}\">", $value);
        }

        foreach ($this->urlAttributes as $urlAttribute) {

            while (preg_match('#'.$urlAttribute.'="([^\d]{1}[\S]{1,})"#is', $value, $matches) && isset($matches[1])){

                $counter++;

                $this->vars["var{$counter}"] = $matches[1];

                $value = Str::replaceFirst("{$urlAttribute}=\"{$matches[1]}\"", "{$urlAttribute}=\"{$counter}\"", $value);

                if($counter > 999){
                    break;
                }
            }

            while (preg_match("#".$urlAttribute."='([^\d]{1}[\S]{1,})'#is", $value, $matches) && isset($matches[1])){

                $counter++;

                $this->vars["var{$counter}"] = $matches[1];

                $value = Str::replaceFirst("{$urlAttribute}='{$matches[1]}'", "{$urlAttribute}='{$counter}'", $value);

                if($counter > 999){
                    break;
                }
            }

            /*foreach ($rules as $rule) {

                while (preg_match($rule, $value, $matches) && isset($matches[1])){

                    $counter++;

                    $this->vars["var{$counter}"] = $matches[1];

                    //\Log::debug($matches[1]);
                    if(Str::contains($matches[1], "River")){\Log::debug($matches[1]);}

                    $value = Str::replaceFirst("{$urlAttribute}=\"{$matches[1]}\"", "{$urlAttribute}=\"{$counter}\"", $value);

                    if($counter > 999){
                        break;
                    }
                }
            }*/
        }

        return $value;
    }

    public function decode($value, $locale = null): string
    {
        $value = preg_replace('#〜#is', '~', $value);
        $value = preg_replace('#：#is', ':', $value);

        $value = preg_replace('#\{ ([\d]{1,3})#is', '{$1', $value);
        $value = preg_replace('#([\d]{1,3}) \}#is', '$1}', $value);

        /*$value = preg_replace('#href="\* ([\d]{1,3})\*"#is', 'href="*$1*"', $value);
        $value = preg_replace('#href="\*([\d]{1,3}) \*"#is', 'href="*$1*"', $value);*/

        $value = preg_replace_callback('#\{([\w\d\s_-]{1,70})\}#is', function ($matches){

            $result = $matches[0];

            if(isset($matches[1])){
                $result = "{".str_replace(' ', '', $matches[1])."}";
            }

            return $result;

        }, $value);

        $value = preg_replace('#(\S)\{([\d]{1,3})#is', '$1 {$2', $value);
        $value = preg_replace('#^\s\{([\d]{1,3})#is', '{$1', $value);

        $value = preg_replace('#([\d]{1,3})\}(\S)#is', '$1} $2', $value);

        $value = preg_replace('#data[\s]{1}-tag#is', 'data-tag', $value);
        $value = preg_replace('#data[\s]{1}-ctag#is', 'data-ctag', $value);

        $value = preg_replace('#data-[\s]{1}tag#is', 'data-tag', $value);
        $value = preg_replace('#data-[\s]{1}ctag#is', 'data-ctag', $value);

        $value = preg_replace('#Data-image#is', 'data-image', $value);
        $value = preg_replace('#Data-tag#is', 'data-tag', $value);

        $value = preg_replace('#Data-image#is', 'data-image', $value);
        $value = preg_replace('#Data-ctag#is', 'data-ctag', $value);

        if(!empty($this->vars)){

            //\Log::debug($this->vars);

            foreach ($this->vars as $key => $var) {

                $counter = (int)preg_replace('#\D#', '', $key);

                $value = str_replace("{{$counter}}", $var, $value);

                foreach ($this->urlAttributes as $urlAttribute) {
                    $value = str_replace("{$urlAttribute}=\"{$counter}\"", "{$urlAttribute}=\" {$var}\"", $value);
                    $value = str_replace("{$urlAttribute}=\"{$counter}\"", "{$urlAttribute}=\"{$var}\"", $value);
                    $value = str_replace("{$urlAttribute}=\"{$counter}\"", "{$urlAttribute}=\"{$var} \"", $value);
                }

                foreach ($this->urlAttributes as $urlAttribute) {
                    $value = str_replace("{$urlAttribute}='{$counter}'", "{$urlAttribute}=' {$var}'", $value);
                    $value = str_replace("{$urlAttribute}='{$counter}'", "{$urlAttribute}='{$var}'", $value);
                    $value = str_replace("{$urlAttribute}='{$counter}'", "{$urlAttribute}='{$var} '", $value);
                }
            }
        }

        foreach ($this->encodeTags as $encodeTag) {

            $value = preg_replace('#<p data-ctag=\"'.$encodeTag.'\">#is', "</{$encodeTag}>", $value);
            $value = preg_replace('#<p data-tag=\"'.$encodeTag.'\">#is', "<{$encodeTag}>", $value);
        }

        $value = preg_replace('#\{colon_([\w_-]{1,50})_colon\}#is', ':$1', $value);
        $value = preg_replace('#\{quad_([\w_-]{1,50})_quad\}#is', '[$1]', $value);
        $value = preg_replace('#\{encoded_([\w_-]{1,50})_encoded\}#is', '{$1}', $value);

        $value = preg_replace_callback('#\{([A-Za-z0-9\._\s]{1,30}):#uis', function ($matches){

            return preg_replace('#\s#uis', '', $matches[0]);

        }, $value);

        $value = html_entity_decode($value);
        $value = htmlspecialchars_decode($value);

        // CSS styles

        $styles = ["width","height"];

        foreach ($styles as $style) {
            $value = preg_replace('#'.$style.'[\s]{1}#isu', $style, $value);
            $value = preg_replace('#'.$style.':[\s]{1}#isu', "{$style}:", $value);
        }

        $value = preg_replace('#px[\s]{1};#isu', "px;", $value);

        return $value;
    }
}
