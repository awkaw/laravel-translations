<?php

namespace LaravelTranslations\Services\GoogleTranslate\Encoders;

use Illuminate\Support\Str;
use LaravelTranslations\Services\GoogleTranslate\Contracts\EncoderContract;

class Img extends Base implements EncoderContract
{
    protected $mapVars = [];

    protected function addToMapVars($key, $value){
        $this->mapVars[$key] = $value;
    }

    private $algorithms = [
        "vars_1" => [
            "encoder" => [
                "rule" => [
                    "\[(.*?)\]" => "<img src2='value'>"
                ]
            ],
            "decoder" => [
                "rule" => [
                    "<img src2='(.*?)'>" => "[value]"
                ]
            ],
            "fix" => [],
        ],
        "vars_2" => [
            "encoder" => [
                "rule" => [
                    "(\{[a-z_]{1,40}\})" => "<img src3='value'>"
                ]
            ],
            "decoder" => [
                "rule" => [
                    "<img src3='(.*?)'>" => "value"
                ]
            ],
            "fix" => [],
        ],
        "vars_3" => [
            "encoder" => [
                "rule" => [
                    "(:[a-z]+)" => "<img src4='value'>"
                ]
            ],
            "decoder" => [
                "rule" => [
                    "<img src4='(.*?)'>" => "value"
                ]
            ],
            "fix" => [],
        ],
        "html" => [
            "encoder" => [
                "rule" => [
                    "<(.*?)>" => "<img src1='value'>"
                ],
                "exclude" => ["src2","src3","src4"]
            ],
            "decoder" => [
                "rule" => [
                    "<img src1='(.*?)'>" => "<value>"
                ],
            ],
            "fix" => [],
        ],
    ];

    private function getNextKeyMapVars(){
        return ("40".(count($this->mapVars) + 1));
    }

    public function encode($text, $locale = null):string
    {
        $text = preg_replace('#&nbsp;#', ' ', $text);

        $this->mapVars = [];

        if(!empty($this->algorithms)) {

            foreach ($this->algorithms as $algorithm) {

                if (
                    isset($algorithm['encoder']['rule'])
                    && is_array($algorithm['encoder']['rule'])
                    && !empty($algorithm['encoder']['rule'])
                ) {

                    foreach ($algorithm['encoder']['rule'] as $from => $to) {

                        $text = preg_replace_callback('#'.$from.'#', function ($matches) use ($to, $algorithm) {

                            if(isset($matches[1])) {

                                $allow = true;

                                if(
                                    isset($algorithm['encoder']['exclude'])
                                    && is_array($algorithm['encoder']['exclude'])
                                    && !empty($algorithm['encoder']['exclude'])
                                ){
                                    foreach ($algorithm['encoder']['exclude'] as $item) {

                                        if(preg_match('#'.$item.'#', $matches[1])){
                                            $allow = false;
                                        }
                                    }
                                }

                                if($allow) {

                                    if (Str::contains($to, "value")) {

                                        $key = $this->getNextKeyMapVars();

                                        $this->addToMapVars($key, $matches[1]);

                                        return str_replace("value", $key, $to);
                                    }
                                }
                            }

                            return $matches[0];

                        }, $text);
                    }
                }
            }
        }

        return $text;
    }

    public function decode($text, $locale = null): string
    {
        $text = preg_replace_callback('#<(.*?)>#', function ($matches) {

            if(isset($matches[1]) && Str::length($matches[1]) > 0) {

                $matches[1] = Str::lower($matches[1]);

                $matches[1] = str_replace(' ', '', $matches[1]);
                $matches[1] = preg_replace('#^img#', 'img ', $matches[1]);

                return "<{$matches[1]}>";
            }

            return $matches[0];

        }, $text);

        if(!empty($this->algorithms)) {

            $mapVars = $this->mapVars;

            foreach ($this->algorithms as $algorithm) {

                if(
                    isset($algorithm['fix'])
                    && is_array($algorithm['fix'])
                    && !empty($algorithm['fix'])
                ){

                    foreach ($algorithm['fix'] as $from => $to) {
                        $text = preg_replace('#'.$from.'#', $to, $text);
                    }
                }

                if (
                    isset($algorithm['decoder']['rule'])
                    && is_array($algorithm['decoder']['rule'])
                    && !empty($algorithm['decoder']['rule'])
                ) {

                    foreach ($algorithm['decoder']['rule'] as $from => $to) {

                        $text = preg_replace_callback('#'.$from.'#', function ($matches) use ($to, $mapVars) {

                            if(isset($matches[1]) && Str::length($matches[1]) > 0) {

                                if(Str::contains($to, "value")){

                                    $result = "";
                                    $key = trim((string)$matches[1]);

                                    if(isset($mapVars[$key])){
                                        $result = $mapVars[$key];
                                    }

                                    return str_replace("value", $result, $to);
                                }
                            }

                            return $matches[0];

                        }, $text);
                    }
                }
            }
        }

        return $text;
    }
}
