<?php

namespace LaravelTranslations\Services\GoogleTranslate\Encoders;

use LaravelTranslations\Services\GoogleTranslate\Contracts\EncoderContract;
use LaravelTranslations\Services\GoogleTranslate\Traits\EncodeDecodeHtmlTrait;

class HtmlStars2 extends Base implements EncoderContract
{
    use EncodeDecodeHtmlTrait;

    private $encoders = [
        [
            "encoder" => [
                "<(.*?)>" => " <* key_value *> " // raw_value or key_value or value
            ],
            "decoder" => [
                "<\*(.*?)\*>" => "<key_value>" // raw_value or key_value or value
            ],
            "fix" => [
                "\*([\d\s]{1,})\*" => "<*$1*>",
                "<<\*" => "<*",
                "\*>>" => "*>",
            ],
        ],
        [
            "encoder" => [
                "\[(.*?)\]" => "[[key_value]]" // raw_value or key_value or value
            ],
            "decoder" => [
                "\[\[(.*?)\]\]" => "[key_value]" // raw_value or key_value or value
            ],
            "fix" => [
                "\[ \[" => "[[",
                "\] \]" => "]]",
            ],
        ],
    ];
}
