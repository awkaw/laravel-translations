<?php

namespace LaravelTranslations\Services\GoogleTranslate\Encoders;

use LaravelTranslations\Services\GoogleTranslate\Contracts\EncoderContract;
use LaravelTranslations\Services\GoogleTranslate\Traits\EncodeDecodeHtmlTrait;

class HtmlStars extends Base implements EncoderContract
{
    use  EncodeDecodeHtmlTrait;

    private $encoders = [
        [
            "encoder" => [
                "<(.*?)>" => " ***key_value*** "
            ],
            "decoder" => [
                "\*\*\*(.*?)\*\*\*" => "<key_value>"
            ],
            "fix" => [

            ],
        ],
        [
            "encoder" => [
                "\[(.*?)\]" => "[[value]]"
            ],
            "decoder" => [
                "\[\[(.*?)\]\]" => "[value]"
            ],
            "fix" => [
                "\[ \[" => "[[",
                "\] \]" => "]]",
            ],
        ],
    ];
}
