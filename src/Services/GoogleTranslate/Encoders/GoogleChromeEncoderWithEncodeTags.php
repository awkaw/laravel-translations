<?php

namespace LaravelTranslations\Services\GoogleTranslate\Encoders;

use LaravelTranslations\Services\GoogleTranslate\Contracts\EncoderContract;

class GoogleChromeEncoderWithEncodeTags extends GoogleChromeEncoder implements EncoderContract
{
    const ENCODE_RULES = [
        "\{[a-zA-Z]{1}(.*?)\}",
        "<a(.*?)>(.*?)<\/a>",
        "<([a-zA-Z]{1,6}[0-9]{0,1})>",
        "<\/([a-zA-Z]{1,6}[0-9]{0,1})>",
    ];

    public function encode($value, $locale = null): string
    {
        $counter = 1000;

        foreach (self::ENCODE_RULES as $rule){

            while (preg_match('#'.$rule.'#is', $value, $matches)){

                $this->vars["var{$counter}"] = $matches[0];

                $value = preg_replace('#'.$rule.'#is', "{".$counter."}", $value, 1);

                $counter++;

                if($counter > 1999){
                    break;
                }
            }
        }

        return parent::encode($value, $locale);
    }

    public function decode($value, $locale = null): string
    {
        return parent::decode(parent::decode($value, $locale), $locale);
    }
}
