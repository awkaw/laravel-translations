<?php


namespace LaravelTranslations\Services\ContentFixedService\Handlers;

use LaravelTranslations\Services\ContentFixedService\Interfaces\HandlerInterface;

class DecodeHtmlHandler implements HandlerInterface
{
    public function handle($value): string
    {
        return html_entity_decode(htmlspecialchars_decode($value));
    }
}
