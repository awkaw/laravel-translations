<?php


namespace LaravelTranslations\Services\ContentFixedService\Handlers;

use LaravelTranslations\Services\ContentFixedService\Interfaces\HandlerInterface;

class FixSpaceHandler implements HandlerInterface
{

    public function handle($value): string
    {
        $value = preg_replace('#&nbsp;#', " ", $value);
        $value = preg_replace('#“#', "\"", $value);
        $value = preg_replace('#”#', "\"", $value);

        $value = preg_replace('#([\S+])[\s]{1,3}=[\s]{1,3}"#', "$1=\"", $value);
        $value = preg_replace('#([\S+])[\s]{1,3}="#', "$1=\"", $value);
        $value = preg_replace('#([\S+])=[\s]{1,3}"#', "$1=\"", $value);

        $value = preg_replace('#([\S+])[\s]{1,3}=[\s]{1,3}\'#', "$1='", $value);
        $value = preg_replace('#([\S+])[\s]{1,3}=\'#', "$1='", $value);
        $value = preg_replace('#([\S+])=[\s]{1,3}\'#', "$1='", $value);

        $patterns = [
            '#=\'(.*?)\'#',
            '#="(.*?)"#',
        ];

        foreach ($patterns as $pattern) {

            preg_match_all($pattern, $value, $matches);

            if(isset($matches[0]) && !empty($matches)){

                foreach ($matches as $match) {

                    $value = str_replace($match, preg_replace('#[\s+]#', '', $match), $value);
                }
            }
        }

        $tagsWithNewLine = ["p","li","ul"];

        $value = preg_replace('#<[a-z]{1,10}>([\s]*)</[a-z]{1,10}>#', "", $value);
        $value = preg_replace("#(<[a-z]{1,10}>)\n#", "$1", $value);
        $value = preg_replace("#\n(<\/[a-z]{1,10}>)#", "$1", $value);
        $value = preg_replace("#<p>#", "\n<p>", $value);

        foreach ($tagsWithNewLine as $item) {
            $value = preg_replace("#<\/{$item}>#", "</{$item}>\n\n", $value);
        }

        $value = preg_replace("#<ol>#", "<ol>\n", $value);
        $value = preg_replace("#<ul>#", "<ul>\n", $value);

        $value = preg_replace("#^\n#", "", $value);
        $value = preg_replace("#\n\n$#", "", $value);
        $value = preg_replace("#\n\n\n#", "\n\n", $value);
        $value = preg_replace("#\n\n\n\n#", "\n\n", $value);
        $value = preg_replace("#><#", "> <", $value);

        return $value;
    }
}
