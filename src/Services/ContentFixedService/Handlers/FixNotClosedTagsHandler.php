<?php


namespace LaravelTranslations\Services\ContentFixedService\Handlers;

use LaravelTranslations\Logger;
use LaravelTranslations\Services\ContentFixedService\ContentFixedService;
use LaravelTranslations\Services\ContentFixedService\Interfaces\HandlerInterface;
use LaravelTranslations\Services\ContentFixedService\Traits\GetTagsTrait;

class FixNotClosedTagsHandler implements HandlerInterface
{
    use GetTagsTrait;

    public function handle($value): string
    {
        $handleTags = ["p","strong","ul","ol","li"];

        foreach ($handleTags as $handleTag) {

            $value = preg_replace('#([^<])\/'.$handleTag.'>#', "$1</{$handleTag}>", $value);
            $value = preg_replace('#<\/'.$handleTag.'([^>])#', "</{$handleTag}>$1", $value);
            $value = preg_replace('#([^<|^\/])'.$handleTag.'>#', "$1<{$handleTag}>", $value);
            $value = preg_replace('#<'.$handleTag.'([^>])#', "<{$handleTag}>$1", $value);

            $value = preg_replace('#<<'.$handleTag.'>#', "<{$handleTag}>", $value);
            $value = preg_replace('#<'.$handleTag.'>>#', "<{$handleTag}>", $value);
            $value = preg_replace('#<\/<\/'.$handleTag.'>#', "</{$handleTag}>", $value);

            $tags = self::getClosableTags($value, [$handleTag]);

            if(!empty($tags)) {

                //Logger::debug($tags);

                $tmp = $value;

                foreach ($tags as $key => $tag) {
                    $tmp = preg_replace('#'.$handleTag.'>#', $handleTag.($key+1).">", $tmp, 1);
                }

                $prevTag = "";

                foreach ($tags as $key => $tag) {

                    $prevTagNumbered = preg_replace('#'.$handleTag.'#', $handleTag.($key), $tag, 1);
                    $tagNumbered = preg_replace('#'.$handleTag.'#', $handleTag.($key+1), $tag, 1);

                    if($key == 0 && $tag == "</{$handleTag}>"){
                        $tmp = "<{$handleTag}>{$tmp}";
                    }

                    if(array_key_last($tags) == $key && $tag == "<{$handleTag}>"){
                        $tmp = "{$tmp}</{$handleTag}>";
                    }

                    if($prevTag == "</{$handleTag}>" && $tag == "</{$handleTag}>"){
                        $tmp = str_replace($prevTagNumbered, "{$tag}<{$handleTag}>", $tmp);
                    }

                    if($prevTag == "<{$handleTag}>" && $tag == "<{$handleTag}>"){
                        $tmp = str_replace($tagNumbered, "</{$handleTag}>{$tag}", $tmp);
                    }

                    $prevTag = $tag;
                }

                foreach ($tags as $key => $tag) {

                    $tagNumbered = preg_replace('#'.$handleTag.'#', $handleTag.($key+1), $tag, 1);

                    $tmp = str_replace($tagNumbered, $tag, $tmp);
                }

                $value = $tmp;
            }
        }

        return $value;
    }
}
