<?php


namespace LaravelTranslations\Services\ContentFixedService\Handlers;

use LaravelTranslations\Services\ContentFixedService\Interfaces\HandlerInterface;

class FixTargetBlankHandler implements HandlerInterface
{
    public function handle($value): string
    {
        return preg_replace('#target=\'_blank\' target="_blank"#', "target=\"_blank\"", $value);
    }
}
