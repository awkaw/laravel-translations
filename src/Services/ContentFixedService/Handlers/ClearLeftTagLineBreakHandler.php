<?php


namespace LaravelTranslations\Services\ContentFixedService\Handlers;

use LaravelTranslations\Services\ContentFixedService\Interfaces\HandlerInterface;

class ClearLeftTagLineBreakHandler implements HandlerInterface
{
    public function handle($value): string
    {
        return preg_replace("#<\n#", "\n", $value);
    }
}
