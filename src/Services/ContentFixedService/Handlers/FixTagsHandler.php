<?php


namespace LaravelTranslations\Services\ContentFixedService\Handlers;

use LaravelTranslations\Services\ContentFixedService\Interfaces\HandlerInterface;
use Illuminate\Support\Str;

class FixTagsHandler implements HandlerInterface
{
    public function handle($value): string
    {
        $value = preg_replace('#<<#', "<", $value);
        $value = preg_replace('#<\/<\/#', "</", $value);

        $value = preg_replace('#<[\s]{0,5}([\S]{1,10})[\s]{0,5}>#', "<$1>", $value);
        $value = preg_replace('#<[\s]{0,5}\/[\s]{0,5}([\S]{1,10})[\s]{0,5}>#', "</$1>", $value);

        $value = preg_replace('#<[\s]{0,2}([\S]{1,10})#', "<$1", $value);

        $value = preg_replace_callback('#(<[\/]?[A-Z]{1,10})#', function ($word) {
            return Str::lower($word[1]);
        }, $value);

        $value = preg_replace('#<<([\S]{1,10})>#', "<$1>", $value);
        $value = preg_replace('#<([\S]{1,10})>>#', "<$1>", $value);

        $value = preg_replace('#<\/<\/([\S]{1,10})>#', "</$1>", $value);
        $value = preg_replace('#>([\s]+)<#', "><", $value);

        return $value;
    }
}
