<?php


namespace LaravelTranslations\Services\ContentFixedService\Traits;


trait HtmlTags
{
    public function getListAllowTags(){

        return [
            "p","div","br","hr","a","span","ul","ol","li","h1","h2","h3","h4","h5","h6",
            "b","strong","i","audio","video","iframe","object","img","picture","label","input","sup","sub",
            "section","html","body","head","table","td","th","tr","thead","tbody","tfoot"
        ];
    }

    public function getListClosableTags(){

        return [
            "p","div","a","span","ul","ol","li","h1","h2","h3","h4","h5","h6",
            "b","strong","i","audio","video","iframe","object","label","sup","sub",
            "section","html","body","head","table","td","th","tr","thead","tbody","tfoot"
        ];
    }

    public function getListTagsWithoutParams(){

        return [
            "p","div","span","ul","ol","li","h1","h2","h3","h4","h5","h6",
            "b","strong","i","sup","sub",
            "section","table","td","th","tr","thead","tbody","tfoot"
        ];
    }
}
