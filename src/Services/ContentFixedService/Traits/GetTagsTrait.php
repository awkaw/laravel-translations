<?php


namespace LaravelTranslations\Services\ContentFixedService\Traits;

use LaravelTranslations\Services\ContentFixedService\ContentFixedService;

trait GetTagsTrait
{
    use HtmlTags;

    private function getClosableTags($value, $filter = [])
    {
        $result = [];

        preg_match_all('#(<[\/]?[a-z]{1,10}[\s|>])#', $value, $matches);

        if (isset($matches[1]) && !empty($matches[1])) {

            foreach ($matches[1] as $key => $match) {

                $matchWithoutSymbols = preg_replace('[\W]', '', $match);

                if (
                    in_array($matchWithoutSymbols, $this->getListClosableTags())
                    && (empty($filter) || in_array($matchWithoutSymbols, $filter))
                ) {
                    $result[] = $match;
                }
            }
        }

        return $result;
    }

    private function getClosableTagsWithPositions($value, $filter = [])
    {
        $result = [];

        $tags = self::getClosableTags($value, $filter);

        if(!empty($tags)){

            $offset = 0;

            foreach ($tags as $tag) {

                $position = stripos($value, $tag, $offset);

                if($position !== false){

                    $result[$position] = $tag;

                    $offset = $position+1;
                }
            }
        }

        return $result;
    }
}
