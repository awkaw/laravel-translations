<?php


namespace LaravelTranslations\Services\ContentFixedService\Interfaces;

interface HandlerInterface
{
    public function handle($value): string;
}
