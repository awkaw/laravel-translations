<?php


namespace LaravelTranslations\Services\ContentFixedService;

use LaravelTranslations\Services\ContentFixedService\Handlers\ClearLeftTagLineBreakHandler;
use LaravelTranslations\Services\ContentFixedService\Handlers\DecodeHtmlHandler;
use LaravelTranslations\Services\ContentFixedService\Handlers\FixNotClosedTagsHandler;
use LaravelTranslations\Services\ContentFixedService\Handlers\FixSpaceHandler;
use LaravelTranslations\Services\ContentFixedService\Handlers\FixTagsHandler;
use LaravelTranslations\Services\ContentFixedService\Handlers\FixTargetBlankHandler;
use LaravelTranslations\Services\ContentFixedService\Interfaces\HandlerInterface;

class ContentFixedService
{
    public static function fixHtml($value){

        $handlers = [
            DecodeHtmlHandler::class,

            FixTagsHandler::class,
            FixNotClosedTagsHandler::class,
            ClearLeftTagLineBreakHandler::class,

            FixSpaceHandler::class,
            FixTargetBlankHandler::class,
        ];

        foreach ($handlers as $handler) {

            $handlerObj = new $handler();

            if($handlerObj instanceof HandlerInterface){
                $value = $handlerObj->handle($value);
            }
        }

        return $value;
    }
}
