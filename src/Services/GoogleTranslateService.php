<?php

namespace LaravelTranslations\Services;

use Illuminate\Support\Str;
use LaravelTranslations\Logger;
use LaravelTranslations\Services\GoogleTranslate\Contracts\ProviderContract;
use LaravelTranslations\Services\GoogleTranslate\Providers\GoogleChromeBrowser;
use LaravelTranslations\Services\GoogleTranslate\Providers\GoogleTranslateApi;

class GoogleTranslateService
{
    private $providerObj = null;
    private $source = "en";
    private $target = "";

    private $options = [
        "provider" => GoogleTranslateApi::class,
        //"provider" => GoogleChromeBrowser::class,
        "googleChromeAddress" => "",
        "delay" => 2,
        "debug" => false,
        "encoder" => "auto"
    ];

    public function __construct(string $source, string $target, $options = [])
    {
        $this->source = $source;
        $this->target = $target;

        $this->options['fixHtml'] = config("translations.fix_html");
        $this->options['debug'] = config("translations.debug");

        $this->options = array_merge_recursive_distinct($this->options, $options);

        $provider = $this->options['provider'];

        $this->providerObj = new $provider($source, $target, $this->options);
    }

    public function getProvider(): ProviderContract {
        return $this->providerObj;
    }

    public function translate($text){

        $result = "";

        if(is_string($text) && Str::length($text) > 0) {

            if ($this->providerObj instanceof ProviderContract) {
                $result = $this->providerObj->translate($text);
            }

            if (is_string($result) && Str::length($result) < 1) {
                throw new \Exception("Error translate");
            }
        }

        return $result;
    }
}
