<?php


namespace LaravelTranslations;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use LaravelTranslations\Providers\Excel\Export;
use LaravelTranslations\Providers\Excel\Import;
//use Maatwebsite\Excel\Facades\Excel;

class Manager
{
    const TRANSLATIONS_EXCEL_FILE = "translations.xlsx"; // project/storage/app/translations.xlsx

    public static function array_merge_recursive_distinct(array &$array1, array &$array2){
        $merged = $array1;

        foreach ($array2 as $key => &$value){
            if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
                $merged[$key] = self::array_merge_recursive_distinct($merged[$key], $value);
            }else{
                $merged[$key] = $value;
            }
        }

        return $merged;
    }

    public static function exportTranslationsFromDatabaseToFiles($locale = null){

        if(is_null($locale)){
            $translations = Translation::all();
        }else{
            $translations = Translation::where("language", $locale)->get();
        }

        return self::putToLangFilesFromCollection($translations);
    }

    private static function putToLangFilesFromCollection(Collection $items){

        if($items instanceof Collection && $items->isNotEmpty()){

            $data = [];

            foreach ($items as $item) {

                if (!isset($data[$item->module])) {
                    $data[$item->module] = [];
                }

                if(!isset($data[$item->module][$item->language])){
                    $data[$item->module][$item->language] = [];
                }

                $tmpArray = [];

                $item->value = preg_replace('#"#', "'", $item->value);

                Arr::set($tmpArray, $item->name, "|" . base64_encode($item->value) . "|");

                $data[$item->module][$item->language] = self::array_merge_recursive_distinct($data[$item->module][$item->language], $tmpArray);
            }

            if(is_array($data) && !empty($data)){

                foreach($data as $category => $arLang){

                    if(is_array($arLang) && $arLang){

                        foreach($arLang as $code => $arValues){

                            $arValues = "<?php\r\n\r\n/* !!! File Generated !!! */\r\n\r\nreturn ".preg_replace('#\[(.*?)\]#', '"$1"', print_r($arValues, true)).";\r\n\r\n?>";
                            $arValues = preg_replace('#"\\n#', "\",\n", $arValues);
                            $arValues = preg_replace('#Array\\n#', "\n", $arValues);
                            $arValues = preg_replace('#\(\\n#', "[\n", $arValues);
                            $arValues = preg_replace('#\)\\n#', "],\n", $arValues);
                            $arValues = preg_replace('#return \\n\[#', "return [\n", $arValues);
                            $arValues = preg_replace('#\],\\n;#', "];", $arValues);

                            $arValues = preg_replace_callback('#=> \|(.*?)\|#', function($matches) {

                                if(isset($matches[1])){
                                    return '=> "'.base64_decode($matches[1]).'",';
                                }else{
                                    return '=> "",';
                                }

                            }, $arValues);

                            $dir = resource_path("lang/{$code}/");

                            if(!is_dir($dir)){
                                mkdir($dir, 0755, true);
                            }

                            $file = $dir.$category.".php";

                            file_put_contents($file, $arValues);
                        }
                    }
                }

                return true;
            }
        }

        return false;
    }

    private static function getListLanguagesFromFiles(){

        $dirs = glob(resource_path("lang/")."*", GLOB_ONLYDIR);

        $languages = [];

        if(!empty($dirs)){

            foreach ($dirs as $dir) {
                $languages[] = str_replace(resource_path("lang/"), "", $dir);
            }
        }

        return $languages;
    }

    private static function getTranslationsFromFiles(){

        $languages = self::getListLanguagesFromFiles();

        $data = [];

        if(!empty($languages)) {

            foreach ($languages as $language) {

                $langFiles = glob(resource_path("lang/") . "{$language}/*");

                if (!empty($langFiles)) {

                    foreach ($langFiles as $langFile) {

                        $pathInfo = pathinfo($langFile);

                        if (!isset($data[$pathInfo['filename']])) {
                            $data[$pathInfo['filename']] = [];
                        }

                        $langFileContent = Arr::dot(require($langFile));

                        if (is_array($langFileContent) && $langFileContent) {

                            foreach ($langFileContent as $key => $item) {

                                if (!isset($data[$pathInfo['filename']][$key])) {
                                    $data[$pathInfo['filename']][$key] = [];
                                }

                                $data[$pathInfo['filename']][$key][$language] = $item;
                            }
                        }
                    }
                }
            }
        }

        return $data;
    }

    public static function exportTranslationsFromFilesToDatabase($locale = null){

        $data = self::getTranslationsFromFiles();

        if(!empty($data)){

            if(is_null($locale)){
                $languages = self::getListLanguagesFromFiles();
            }else{
                $languages = [$locale];
            }

            if(!empty($languages)){

                foreach($languages as $language){

                    Translation::where("language", $language)->delete();

                    foreach ($data as $module => $items) {

                        if(is_array($items) && count($items) > 0){

                            foreach ($items as $name => $value) {

                                if(isset($value[$language]) && !is_array($value[$language])){

                                    if(!preg_match('#\.[0-9]#', $name)){

                                        $model = new Translation();

                                        $model->module = $module;
                                        $model->name = $name;
                                        $model->language = $language;
                                        $model->value = $value[$language];

                                        $model->save();
                                    }
                                }
                            }
                        }
                    }
                }

                return true;
            }
        }

        return false;
    }

    public static function syncTranslations($locale = null){

        $data = self::getTranslationsFromFiles();

        if(!empty($data)){

            if(is_null($locale)){
                $languages = self::getListLanguagesFromFiles();
            }else{
                $languages = [$locale];
            }

            if(!empty($languages)){

                foreach($languages as $language){

                    foreach ($data as $module => $items) {

                        if(is_array($items) && count($items) > 0){

                            foreach ($items as $name => $value) {

                                if(isset($value[$language]) && !is_array($value[$language])){

                                    if(!Translation::where("module", $module)->where("name", $name)->where("language", $language)->exists()){

                                        $model = new Translation();

                                        $model->module = $module;
                                        $model->name = $name;
                                        $model->language = $language;
                                        $model->value = $value[$language];

                                        $model->save();
                                    }
                                }
                            }
                        }
                    }
                }

                return self::exportTranslationsFromDatabaseToFiles($locale);
            }
        }

        return false;
    }

    public static function exportTranslationsFromFilesToExcel(){

        $data = self::getTranslationsFromFiles();

        if(!empty($data)) {

            $languages = self::getListLanguagesFromFiles();

            if (!empty($languages)) {

                $collection = new Collection();

                $fields = ["module","name"];

                foreach($languages as $language){
                    $fields[] = "value_{$language}";
                }

                $collection->add($fields);

                $result = [];

                foreach($languages as $language){

                    foreach ($data as $module => $items) {

                        if(is_array($items) && count($items) > 0){

                            foreach ($items as $name => $value) {

                                if(isset($value[$language]) && !is_array($value[$language])){

                                    if(!preg_match('#\.[0-9]#', $name)){

                                        if(!isset($result[$module])){
                                            $result[$module] = [];
                                        }

                                        if(!isset($result[$module][$name])){
                                            $result[$module][$name] = [];
                                        }

                                        $result[$module][$name][$language] = $value[$language];
                                    }
                                }
                            }
                        }
                    }
                }

                if(!empty($result)){

                    foreach ($result as $module => $names) {

                        if(is_array($names) && !empty($names)){

                            foreach ($names as $name => $langValues) {

                                $tmp = [$module, $name];

                                foreach($languages as $language){

                                    if(isset($langValues[$language])){
                                        $tmp[] = $langValues[$language];
                                    }else{
                                        $tmp[] = "";
                                    }
                                }

                                $collection->add($tmp);
                            }
                        }
                    }
                }

                return false;

                //return Excel::store(new Export($collection), self::TRANSLATIONS_EXCEL_FILE);
            }
        }

        return false;
    }

    public static function exportTranslationsFromExcelToFiles(){

        Logger::debug("exportTranslationsFromExcelToFiles");

        //$array = Excel::toArray(null, self::TRANSLATIONS_EXCEL_FILE);
        $array = [];

        if(isset($array[0]) && isset($array[0][0])){

            $map = [];
            $languages = [];
            $collection = new Collection();

            foreach ($array[0][0] as $key => $item) {

                if($item == "module"){
                    $map["module"] = $key;
                }

                if($item == "name"){
                    $map["name"] = $key;
                }

                if(preg_match('#value_#', $item)){

                    $arItem = explode("_", $item);

                    if(isset($arItem[1])){

                        $map[$arItem[1]] = $key;

                        if(!in_array($arItem[1], $languages)){
                            $languages[] = $arItem[1];
                        }
                    }
                }

            }

            foreach ($array[0] as $key => $item) {

                if($key > 0){

                    foreach ($languages as $language) {

                        $model = new Import();

                        $model->module = $item[$map["module"]];
                        $model->name = $item[$map["name"]];
                        $model->language = $language;
                        $model->value = $item[$map[$language]];

                        $collection->add($model);
                    }
                }
            }

            return self::putToLangFilesFromCollection($collection);
        }

        return false;
    }
}
