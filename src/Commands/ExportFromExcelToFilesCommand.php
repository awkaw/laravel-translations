<?php


namespace LaravelTranslations\Commands;

use Illuminate\Console\Command;
use LaravelTranslations\Manager;

class ExportFromExcelToFilesCommand extends Command{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'laravelTranslations:exportFromExcelToFiles';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Export from excel to files';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		return Manager::exportTranslationsFromExcelToFiles();
	}
}
