<?php

namespace LaravelTranslations\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use LaravelTranslations\Manager;

class ExportFromDatabaseToFilesCommand extends Command{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'laravelTranslations:exportFromDatabaseToFiles {--locale}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Export from database to files';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
	    $locale = ($this->hasOption("locale") && Str::length($this->option("locale")) > 0) ? $this->option("locale") : null;

		return Manager::exportTranslationsFromDatabaseToFiles($locale);
	}
}
