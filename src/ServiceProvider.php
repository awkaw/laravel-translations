<?php

namespace LaravelTranslations;

use LaravelTranslations\Commands\ExportFromDatabaseToFilesCommand;
use LaravelTranslations\Commands\ExportFromExcelToFilesCommand;
use LaravelTranslations\Commands\ExportFromFilesToDatabaseCommand;
use LaravelTranslations\Commands\ExportFromFilesToExcelCommand;
use LaravelTranslations\Commands\SyncTranslationsCommand;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            ExportFromFilesToDatabaseCommand::class,
            ExportFromDatabaseToFilesCommand::class,
            ExportFromFilesToExcelCommand::class,
            ExportFromExcelToFilesCommand::class,
            SyncTranslationsCommand::class,
        ]);

        $this->loadMigrationsFrom(__DIR__."/../migrations");
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/translations.php', 'translates'
        );
    }
}
