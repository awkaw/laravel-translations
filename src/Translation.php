<?php


namespace LaravelTranslations;


use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $table = "translations";
    protected $fillable = ["module","name","language","value"];
}
