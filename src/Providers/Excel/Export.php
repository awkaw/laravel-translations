<?php


namespace LaravelTranslations\Providers\Excel;


use Illuminate\Database\Eloquent\Collection;
/*use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;*/

class Export /*implements FromCollection, ShouldAutoSize*/
{
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function collection()
    {
        return $this->collection;
    }
}
