<?php


namespace LaravelTranslations\Providers\Excel;


use Illuminate\Support\Collection;
use LaravelTranslations\Logger;
/*use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;*/

class Import /*implements ToModel, ToArray, ToCollection*/
{
    public function model(array $row){

        Logger::debug($row);

        return null;
    }

    public function array(array $array)
    {
        Logger::debug($array);

        return null;
    }

    public function collection(Collection $collection)
    {

        Logger::debug($collection);

        return null;
    }
}
