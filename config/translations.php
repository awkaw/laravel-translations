<?php

return [
    "debug" => false,
    "google" => [
        "translate" => [
            "delay_for_request" => 2
        ]
    ],
    "fix_html" => true
];
